# Golang_OpenGL_dev ![Language](https://img.shields.io/badge/Language-Golang%20GLSL-blue.svg) ![License](https://img.shields.io/badge/License-BSD%203--Clause-brightgreen.svg)

### 写在前面
学Golang断断续续也有一段时间了，一直想写个gui程序，找到了[go-ui](https://github.com/andlabs/ui)这个库，写ui很简单，但是又发现无法做OpenGL，于是又找到了[go-glfw](https://github.com/go-gl/glfw)，发现与[OpenGL教程](http://bullteacher.com/category/zh_learnopengl_com)什么写的非常匹配，除了个别c++和Golang之间的细微差别，其他没有什么特别多的问题，同时也参考了[go-glfw的examples](https://github.com/go-gl/examples)，当然例子里面的图形比较复杂并且也使用了图片纹理(texture)，我提取了其中编译绑定着色器的函数封装_compileShader.go_，并且将顶点着色器和像素着色器分离到单独的文件中（*fragmentShader.fs*和*vertexShader.vs*）。

### 2016年9月26日21:03:42
发现了第一个坑，可能是Golang或是OpenGL基础的问题，问题出现在使用`gl.DrawElements`绘制**索引缓冲对象**的最后一个参数是`gl.PtrOffset`类型的，需要用强制类型转换，因为Golang的指针偏移是一个专用类型，在c或是c++是int类型

### 2016年9月27日21:37:48
用`gl.DrawElements`画图时可以使用最后一个参数指定偏移量，这个偏移量是相对于**索引缓冲对象**的起始位置的，起始位置为`0`，偏移量单位是`byte`，一般是3*4的倍数。使用偏移量可以和第二个参数`count int32`配合指定所要画的图形。

### 2016年9月28日09:01:06
开始系统学习GLSL，现在简单理解**顶点着色器**是接受顶点数据，转换或者重新排列顶点位置信息、颜色和其他信息的处理程序，可以通过`in`关键字定义OpenGL输入的顶点属性，`gl_Position`这个内置变量会把位置输出到正式的设备坐标上。而**像素着色器**则是对每一个顶点的颜色信息作处理然后输出到正式的像素颜色上面。

### 2016年10月11日08:57:32
发现在Sublime Text中有一个GLSL插件可以给`.fs``.vs`等文件高亮语法显示，我看教程介绍可以把着色器程序直接放在程序中以字符串形式调用，但是我还是准备设计一下把着色器放在的单独的文件之中，但是一个文件里面可以放多个着色器代码，用特殊的注释符号分割开，调用的时候只需要`load(shaderName)`就可以了这样就会方便很多

### 现阶段屏幕截图
![Screenshot](https://git.oschina.net/yumisanxiang/Golang_OpenGL_dev/raw/master/Screenshot.png)